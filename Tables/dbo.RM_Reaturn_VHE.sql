CREATE TABLE [dbo].[RM_Reaturn_VHE]
(
[Complex] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Name] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Label] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FromSnapshotNumber] [int] NULL,
[ThruSnapshotNumber] [int] NULL,
[IntCol] [int] NULL,
[DoubleCol] [float] NULL,
[StrCol] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DTCol] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
