CREATE TABLE [dbo].[RM_Reaturn_Complex_eenheden]
(
[Complex] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VHE] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FromSnapshotNumber] [int] NULL,
[ThruSnapshotNumber] [int] NULL
) ON [PRIMARY]
GO
