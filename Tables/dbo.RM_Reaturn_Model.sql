CREATE TABLE [dbo].[RM_Reaturn_Model]
(
[Complex] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Label] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FromSnapshotNumber] [smallint] NOT NULL,
[ThruSnapshotNumber] [smallint] NULL,
[IntCol] [int] NULL,
[DoubleCol] [float] NULL,
[StrCol] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DTCol] [date] NULL,
[FromYear] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ThruYear] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
