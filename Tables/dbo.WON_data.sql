CREATE TABLE [dbo].[WON_data]
(
[createdate] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Complex] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Vhe] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Postcode] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Huisnummer] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Huisnummer_toevoeging] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Complexvariant_code] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Complexvariant_naam] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Vhe_nummer] [bigint] NULL,
[DAEB] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
