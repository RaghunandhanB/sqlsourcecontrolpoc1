CREATE TABLE [dbo].[RM_Reaturn_Ruimte]
(
[Complex] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Label] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FromSnapshotNumber] [smallint] NOT NULL,
[ThruSnapshotNumber] [smallint] NULL,
[IntCol] [int] NULL,
[DoubleCol] [float] NULL,
[StrCol] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DTCol] [date] NULL
) ON [PRIMARY]
GO
