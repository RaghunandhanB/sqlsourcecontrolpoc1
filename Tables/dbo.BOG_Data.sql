CREATE TABLE [dbo].[BOG_Data]
(
[VHE_001_Klant] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TaxatieRonde] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VHE_101_Vhenummer] [bigint] NULL,
[VHE_102_Complexnummer] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VHE_340_Segmentatie] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VHE_103_Postcode] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VHE_104_Straatnaam] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VHE_105_Huisnummer] [int] NULL,
[VHE_106_Huisnummer_toevoeging] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VHE_112_Complexnaam] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VHE_251_Is_leegstand] [int] NULL,
[VHE_300_Vastgoedtype] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VHE_311_Subtype] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VHE_315_VHE_type] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VHE_503_IsDaeb] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
