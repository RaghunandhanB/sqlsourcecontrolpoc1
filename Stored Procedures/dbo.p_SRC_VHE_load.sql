SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

---- =============================================
---- Author       : Raghu B
---- Create date  : 201111
---- Description  : Procedure to load SRC VHE table from the source data
---- Example call : EXEC [dbo].[p_SRC_VHE_load] @Boekjaar=2018,@UploadID=201113
---- =============================================

CREATE  PROC [dbo].[p_SRC_VHE_load]
	@Boekjaar int,
	@UploadID bigint
as

--Delete table data if exists : 
--Declare @Boekjaar int =2018,@UploadID bigint =201113
delete [Toolbox_RM].[SRC].[Vhe] where UploadID = @UploadID
delete [Toolbox_RM].[SRC].[Boekjaar] where UploadID = @UploadID
delete [Toolbox_RM].[SRC].[Upload] where ID = @UploadID

--Drop Temp table if exists : 
IF OBJECT_ID('tempdb..##RM_Reaturn_VHE_load') is not null 
	BEGIN
		DROP TABLE ##RM_Reaturn_VHE_load
	END

IF OBJECT_ID('tempdb..##RM_Reaturn_Ruimte_load') is not null 
	BEGIN
		DROP TABLE ##RM_Reaturn_Ruimte_load
	END

IF OBJECT_ID('tempdb..##Ruimte_load_rank') is not null 
	BEGIN
		DROP TABLE ##Ruimte_load_rank
	END

IF OBJECT_ID('tempdb..##VHE_load_rank') is not null 
	BEGIN
		DROP TABLE ##VHE_load_rank
	END

--Insert data to Upload table :

Insert	[Toolbox_RM].[SRC].[Upload] ([ID], [Klant], [Boekjaar], [VersieID])
select	@UploadID, [Name], @Boekjaar, 1
from	[dbo].[RM_Reaturn_Model]
where	cast(DoubleCol as int) = 100*(@Boekjaar%100) + 12

--Insert data to Boekjaar table :

Insert	[Toolbox_RM].[SRC].[Boekjaar] ([UploadID], [Klant], [Boekjaar])
select	@UploadID, [Name], @Boekjaar
from	[dbo].[RM_Reaturn_Model]
where	cast(DoubleCol as int) = 100*(@Boekjaar%100) + 12

-- Insert data to Temp table from Reaturn Ruimte table : 

select * 
  into ##RM_Reaturn_Ruimte_load
from [Toolbox_RM_SRC].[dbo].[RM_Reaturn_Ruimte] 
  where Label in ('Vvo','Mutatiehuur','Maandhuur','Leegwaarde')

--Get latest values for Ruimte labels and Insert into Temp table :
--Declare @Boekjaar int =2018,@UploadID bigint =201113

select 	distinct
		@UploadID							as [UploadID],
		left(T1.Name,11)					as [Vhenummer],
		T7.[VHE_103_Postcode]				as [Postcode],
		T7.[VHE_104_Straatnaam]				as [Straatnaam],
		T7.[VHE_105_Huisnummer]				as [Huisnummer],
		T7.[VHE_106_Huisnummer_toevoeging]	as [Huisnummer toevoeging],
		T8.Complex							as [Waarderingscomplex],
		T8.Complex							as [Financieel complex],
		NULL								as [IsDaeb],
		'BOG'								as [BOG of WON],
		NULL								as [AantalVhe],
		T5.DoubleCol						as [GBO oppervlakte per vhe],
		T6.DoubleCol						as [Huurprijs per vhe per maand],
		T3.DoubleCol						as [Leegwaarde per vhe],
		T4.DoubleCol						as [Markthuur per vhe per maand],
		NULL								as [Datum in exploitatie],
		NULL								as [Datum uit exploitatie],
		NULL								as [Reden mutatie],
		GETDATE()							as [created_dttm],
		rank () over (partition by T1.Name,T1.label,T2.thruyear order by ISNULL(T2.[ThruSnapshotNumber], 999) desc) as Ruimte_Rank 
		
	INTO ##Ruimte_load_rank
from 	##RM_Reaturn_Ruimte_load T1

	LEFT JOIN [Toolbox_RM_SRC].[dbo].[RM_Reaturn_Model] T2
		on T2.FromSnapshotNumber between T1.FromSnapshotNumber and ISNULL(T1.ThruSnapshotNumber, 999)
	
	LEFT JOIN ##RM_Reaturn_Ruimte_load T3
		ON T1.Name = T3.NAME
		and T2.FromSnapshotNumber between T3.FromSnapshotNumber and ISNULL(T3.ThruSnapshotNumber, 999)
		and T3.Label  = 'Leegwaarde'
	
	LEFT JOIN ##RM_Reaturn_Ruimte_load T4
		ON T1.Name = T4.NAME
		and T2.FromSnapshotNumber between T4.FromSnapshotNumber and ISNULL(T4.ThruSnapshotNumber, 999)
		and T4.Label  = 'Mutatiehuur'

	LEFT JOIN ##RM_Reaturn_Ruimte_load T5
		ON T1.Name = T5.NAME
		and T2.FromSnapshotNumber between T5.FromSnapshotNumber and ISNULL(T5.ThruSnapshotNumber, 999)
		and T5.Label  = 'Vvo'

	LEFT JOIN ##RM_Reaturn_Ruimte_load T6
		ON T1.Name = T6.NAME
		and T2.FromSnapshotNumber between T6.FromSnapshotNumber and ISNULL(T6.ThruSnapshotNumber, 999)
		and T6.Label  = 'Maandhuur'

	LEFT JOIN [Toolbox_RM_SRC].[dbo].[BOG_Data] T7
		ON left(T1.Name,11) = T7.[VHE_101_Vhenummer]

	LEFT JOIN [dbo].[RM_Reaturn_Complex_eenheden] T8
		ON T1.Name = T8.VHE
		and T1.FromSnapshotNumber between T8.FromSnapshotNumber and ISNULL(T8.ThruSnapshotNumber, 999)

where	2000 + cast(T2.DoubleCol / 100 as int) = @Boekjaar

--Delete Records which are not having the latest values :

Delete from ##Ruimte_load_rank where Ruimte_Rank<>1

-- Insert data to Temp table from Reaturn VHE table : 

select * 
  into ##RM_Reaturn_VHE_load
from [Toolbox_RM_SRC].[dbo].[RM_Reaturn_VHE]
  where Label in ('Gbo m²','Mutatiehuur','Maandhuur','Leegwaarde')

----Get latest values for VHE labels and Insert into Temp table :
--Declare @Boekjaar int =2018,@UploadID bigint =201113

select 	distinct
		@UploadID							as [UploadID],
		left(T1.Name,11)					as [Vhenummer],
		T7.[Postcode]						as [Postcode],
		NULL								as [Straatnaam],
		T7.[Huisnummer]						as [Huisnummer],
		T7.[Huisnummer_toevoeging]			as [Huisnummer toevoeging],
		T8.Complex							as [Waarderingscomplex],
		T8.Complex							as [Financieel complex],
		NULL								as [IsDaeb],
		'WON'								as [BOG of WON],
		NULL								as [AantalVhe],
		T5.DoubleCol						as [GBO oppervlakte per vhe],
		T6.DoubleCol						as [Huurprijs per vhe per maand],
		T3.DoubleCol						as [Leegwaarde per vhe],
		T4.DoubleCol						as [Markthuur per vhe per maand],
		NULL								as [Datum in exploitatie],
		NULL								as [Datum uit exploitatie],
		NULL								as [Reden mutatie],
		GETDATE()							as [created_dttm],
		rank () over (partition by T1.Name,T1.label,T2.thruyear order by ISNULL(T2.[ThruSnapshotNumber], 999) desc) as VHE_Rank 
		into ##VHE_load_rank

from 	##RM_Reaturn_VHE_load T1

	LEFT JOIN [Toolbox_RM_SRC].[dbo].[RM_Reaturn_Model] T2
		on T2.FromSnapshotNumber between T1.FromSnapshotNumber and ISNULL(T1.ThruSnapshotNumber, 999)
	
	LEFT JOIN ##RM_Reaturn_VHE_load T3
		ON T1.Name = T3.NAME
		and T2.FromSnapshotNumber between T3.FromSnapshotNumber and ISNULL(T3.ThruSnapshotNumber, 999)
		and T3.Label  = 'Leegwaarde'
	
	LEFT JOIN ##RM_Reaturn_VHE_load T4
		ON T1.Name = T4.NAME
		and T2.FromSnapshotNumber between T4.FromSnapshotNumber and ISNULL(T4.ThruSnapshotNumber, 999)
		and T4.Label  = 'Mutatiehuur'

	LEFT JOIN ##RM_Reaturn_VHE_load T5
		ON T1.Name = T5.NAME
		and T2.FromSnapshotNumber between T5.FromSnapshotNumber and ISNULL(T5.ThruSnapshotNumber, 999)
		and T5.Label  = 'Gbo m²'

	LEFT JOIN ##RM_Reaturn_VHE_load T6
		ON T1.Name = T6.NAME
		and T2.FromSnapshotNumber between T6.FromSnapshotNumber and ISNULL(T6.ThruSnapshotNumber, 999)
		and T6.Label  = 'Maandhuur'

	LEFT JOIN [Toolbox_RM_SRC].[dbo].[WON_data] T7
		on left(T1.Name,11) = T7.Vhe

	LEFT JOIN [dbo].[RM_Reaturn_Complex_eenheden] T8
		on T1.Name = T8.VHE
		and T1.FromSnapshotNumber between T8.FromSnapshotNumber and ISNULL(T8.ThruSnapshotNumber, 999)

where	2000 + cast(T2.DoubleCol / 100 as int) = @Boekjaar

--Delete Records which are not having the latest values :

Delete from ##VHE_load_rank where VHE_Rank<>1

--Insert into Main Target VHE table :

Insert INTO [Toolbox_RM].[SRC].[Vhe]
SELECT [UploadID]
      ,[Vhenummer]
      ,[Postcode]
      ,[Straatnaam]
      ,[Huisnummer]
      ,[Huisnummer toevoeging]
      ,[Waarderingscomplex]
      ,[Financieel complex]
      ,[IsDaeb]
      ,[BOG of WON]
      ,[AantalVhe]
      ,[GBO oppervlakte per vhe]
      ,[Huurprijs per vhe per maand]
      ,[Leegwaarde per vhe]
      ,[Markthuur per vhe per maand]
      ,[Datum in exploitatie]
      ,[Datum uit exploitatie]
      ,[Reden mutatie]
      ,[created_dttm] 
	FROM ##VHE_load_rank 


Insert INTO [Toolbox_RM].[SRC].[Vhe]
SELECT [UploadID]
      ,[Vhenummer]
      ,[Postcode]
      ,[Straatnaam]
      ,[Huisnummer]
      ,[Huisnummer toevoeging]
      ,[Waarderingscomplex]
      ,[Financieel complex]
      ,[IsDaeb]
      ,[BOG of WON]
      ,[AantalVhe]
      ,[GBO oppervlakte per vhe]
      ,[Huurprijs per vhe per maand]
      ,[Leegwaarde per vhe]
      ,[Markthuur per vhe per maand]
      ,[Datum in exploitatie]
      ,[Datum uit exploitatie]
      ,[Reden mutatie]
      ,[created_dttm] 
	FROM ##Ruimte_load_rank
GO
