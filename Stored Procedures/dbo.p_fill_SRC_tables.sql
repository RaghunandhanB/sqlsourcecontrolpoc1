SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   proc [dbo].[p_fill_SRC_tables]
	@Boekjaar int,
	@UploadID bigint
AS
/*
Description:
Copies the data from a Reaturn data extract to a SRC Upload/Dataset in Toolbox RM;
Reaturn_Model contains monthly snapshots, this procedure copies the data for one @Boekjaar
For SRC.VHE only the latest December snapshot of @Boekjaar is used
For SRC.Grootboek all monthly snapshots of @Boekjaar are used as periods

Example call:
exec p_fill_SRC_tables @Boekjaar=2017,@UploadID=2017
select * from [Toolbox_RM].[SRC].[Upload] where ID=2017
select * from [Toolbox_RM].[SRC].[Accountschema] where UploadID=2017
select * from [Toolbox_RM].[SRC].[Kostenplaats] where UploadID=2017
select * from [Toolbox_RM].[SRC].[Grootboek] where UploadID=2017
select * from [Toolbox_RM].[SRC].[VHE] where UploadID=2017--53466
*/

delete [Toolbox_RM].[SRC].[Vhe] where UploadID = @UploadID
delete [Toolbox_RM].[SRC].[Grootboek] where UploadID = @UploadID
delete [Toolbox_RM].[SRC].[Accountschema] where UploadID = @UploadID
delete [Toolbox_RM].[SRC].[Kostenplaats] where UploadID = @UploadID
delete [Toolbox_RM].[SRC].[Boekjaar] where UploadID = @UploadID
delete [Toolbox_RM].[SRC].[Upload] where ID = @UploadID

insert	[Toolbox_RM].[SRC].[Upload] ([ID], [Klant], [Boekjaar], [VersieID])
select	@UploadID, replace([Name], 'RM_','') , @Boekjaar, 1
from	[dbo].[RM_Reaturn_Model]
where	cast(DoubleCol as int) = 100*(@Boekjaar%100) + 12

insert	[Toolbox_RM].[SRC].[Boekjaar] ([UploadID], [Klant], [Boekjaar])
select	@UploadID, replace([Name], 'RM_',''), @Boekjaar
from	[dbo].[RM_Reaturn_Model]
where	cast(DoubleCol as int) = 100*(@Boekjaar%100) + 12

-- SRC.Kostenplaats (Complex)
INSERT	[Toolbox_RM].[SRC].[Kostenplaats] ([UploadID], [Kostenplaats code], [Kostenplaats omschrijving])
SELECT	DISTINCT @UploadID, left(C.[Name],45) + right(c.[Name],5), C.[Name] --RTRIM(LEFT(C.[Name], CHARINDEX('-', C.[Name]) - 1)), LTRIM(SUBSTRING(C.[Name], CHARINDEX('-', C.[Name]) + 1, 500))
FROM	[dbo].[RM_Reaturn_Model] M
		INNER JOIN [dbo].[RM_Reaturn_Complex] C on M.FromSnapshotNumber between C.FromSnapshotNumber and isnull(C.ThruSnapshotNumber, 999)
WHERE	2000 + cast(M.DoubleCol / 100 as int) = @Boekjaar

-- SRC.Grootboek
-- One record per Period (=Snapshot) per Account (=Label) per Kostenplaats (=Complex)
insert	[Toolbox_RM].[SRC].[Grootboek] ([UploadID], [Periode], [Accountnummer], [Kostenplaats], [Bedrag])
select	@UploadID,
		200000 + cast(M.DoubleCol as int),
		V.[Label],
		left(CV.Complex,45) + right(CV.Complex,5),
		sum(V.DoubleCol)
from	[dbo].[RM_Reaturn_Model] M
			inner join [dbo].[RM_Reaturn_Ruimte] V 
				on M.FromSnapshotNumber between V.FromSnapshotNumber 
				and isnull(V.ThruSnapshotNumber, 999)
			inner join [dbo].[RM_Reaturn_Complex_eenheden] CV 
				on M.FromSnapshotNumber between CV.FromSnapshotNumber 
				and isnull(CV.ThruSnapshotNumber, 999)
				and V.[Name] = CV.VHE
where	2000 + cast(M.DoubleCol / 100 as int) = @Boekjaar
 and	V.Label in (
'Bruto huuropbrengsten',-- instead of theoritische maandhuur
'Boekwaarde',
'Resultaat herwaardering',
'Totale waardegroei',
'Investering',
'Resultaat verkoop',
'Planmatig onderhoud',
'Beheervergoeding',
'Desinvestering',
'Niet-planmatig onderhoud',
'Mutatie- en inrichtingskosten',
'Financiële leegstand',
'Overige opbrengsten',
'Huurvrije periode / huurkorting',
'OZB',
'Oninbare huur',
'Overige belastingen',
'Overige exploitatiekosten',
'Verhuurdersheffing',
'Bijdrage vveBijdrage vve',
'Leefbaarheidsuitgaven',
'Verzekering',
'Advies- en taxatiekosten',
'Kosten tijdens leegstand')
-- and	rtrim(left(C.Name, charindex('-', C.Name) - 1)) = '100.810000'
group by M.DoubleCol, CV.Complex, V.Label

-- SRC.Grootboek (Vhe)
insert	[Toolbox_RM].[SRC].[Grootboek] ([UploadID], [Periode], [Accountnummer], [Kostenplaats], [Bedrag])
select	@UploadID,
		200000 + cast(M.DoubleCol as int),
		V.[Label],
		left(CV.[Complex],45) + right(CV.[Complex],5),--RTRIM(LEFT(C.[Name], CHARINDEX('-', C.[Name]) - 1)),
		sum(V.DoubleCol)
from	[dbo].[RM_Reaturn_Model] M
			inner join [dbo].[RM_Reaturn_Vhe] V 
				on M.FromSnapshotNumber between V.FromSnapshotNumber 
				and isnull(V.ThruSnapshotNumber, 999)
			inner join [dbo].[RM_Reaturn_Complex_eenheden] CV 
				on M.FromSnapshotNumber between CV.FromSnapshotNumber 
				and isnull(CV.ThruSnapshotNumber, 999)
				and V.[Name] = CV.VHE
where	2000 + cast(M.DoubleCol / 100 as int) = @Boekjaar
 and	V.Label in (
'Bruto huuropbrengsten', -- instead of theoritische maandhuur
'Boekwaarde',
'Resultaat herwaardering',
'Totale waardegroei',
'Investering',
'Resultaat verkoop',
'Planmatig onderhoud',
'Beheervergoeding',
'Desinvestering',
'Niet-planmatig onderhoud',
'Mutatie- en inrichtingskosten',
'Financiële leegstand',
'Overige opbrengsten',
'Huurvrije periode / huurkorting',
'OZB',
'Oninbare huur',
'Overige belastingen',
'Overige exploitatiekosten',
'Verhuurdersheffing',
'Bijdrage vveBijdrage vve',
'Leefbaarheidsuitgaven',
'Verzekering',
'Advies- en taxatiekosten',
'Kosten tijdens leegstand')
-- and	rtrim(left(C.Name, charindex('-', C.Name) - 1)) = '100.810000'
group by M.DoubleCol, CV.Complex, V.Label

-- SRC.Account (DISTINCT Account FROM SRC.Grootboek)
INSERT	[Toolbox_RM].[SRC].[Accountschema] ([UploadID], [Accountnummer], [Account omschrijving])
SELECT	DISTINCT @UploadID, Accountnummer, Accountnummer
FROM	[Toolbox_RM].[SRC].[Grootboek]
WHERE	UploadID = @UploadID

-- SRC.VHE ([BOG of WON]=WON)
INSERT	[Toolbox_RM].[SRC].[Vhe] ([UploadID], [Vhenummer], [Postcode], [Straatnaam], [Huisnummer], [Huisnummer toevoeging], [Waarderingscomplex], [Financieel complex], [IsDaeb], [BOG of WON], [AantalVhe], [GBO oppervlakte per vhe], [Huurprijs per vhe per maand], [Leegwaarde per vhe], [Markthuur per vhe per maand], [Datum in exploitatie], [Datum uit exploitatie], [Reden mutatie])
SELECT	@UploadID,
		RTRIM(LEFT(VheNaam, CHARINDEX('-', VheNaam) - 1)) AS [Vhenummer],
		NULL AS [Postcode],
		LTRIM(SUBSTRING(VheNaam, CHARINDEX('-', VheNaam) + 1, 500)) AS [Straatnaam],
		NULL AS [Huisnummer],
		NULL AS [Huisnummer toevoeging],
		left(ComplexNaam,45) + right(ComplexNaam,5) AS [Waarderingscomplex],
		left(ComplexNaam,45) + right(ComplexNaam,5) AS [Financieel complex],--RTRIM(LEFT(ComplexNaam, CHARINDEX('-', ComplexNaam) - 1)) AS [Financieel complex],
		NULL AS [IsDaeb],
		'WON' AS [BOG of WON],
		1 AS [AantalVhe],
		[Gbo m²] AS [GBO oppervlakte per vhe],
		[Maandhuur] AS [Huurprijs per vhe per maand],
		[Leegwaarde] AS [Leegwaarde per vhe],
		[Mutatiehuur] AS [Markthuur per vhe per maand],
		NULL AS [Datum in exploitatie],
		NULL AS [Datum uit exploitatie],
		NULL AS [Reden mutatie]
FROM	(
SELECT	*,
		ROW_NUMBER() OVER (PARTITION BY VheNaam ORDER BY [Period] DESC) AS rownbr
FROM	(
			SELECT	cast(M.DoubleCol / 100 as int) AS [Period], CV.[Complex] AS ComplexNaam, V.[Name] AS VheNaam, V.[Label], V.DoubleCol
			FROM	[dbo].[RM_Reaturn_Model] M
			inner join [dbo].[RM_Reaturn_Vhe] V 
				on M.FromSnapshotNumber between V.FromSnapshotNumber 
				and isnull(V.ThruSnapshotNumber, 999)
			inner join [dbo].[RM_Reaturn_Complex_eenheden] CV 
				on M.FromSnapshotNumber between CV.FromSnapshotNumber 
				and isnull(CV.ThruSnapshotNumber, 999)
				and V.[Name] = CV.VHE
			WHERE	2000 + cast(M.DoubleCol / 100 as int) = @Boekjaar
			 AND	V.[Label] in ('Gbo m²', 'Maandhuur', 'Leegwaarde', 'Mutatiehuur', 'Boekwaarde')
		) T
PIVOT
(
	MAX(DoubleCol)
	FOR [Label] in ([Gbo m²], [Maandhuur], [Leegwaarde], [Mutatiehuur], [Boekwaarde])
) AS pvt
) T
WHERE	rownbr = 1 and [Boekwaarde] <> 0

--Update House address Details of WON: BM201124 : FIXED, MORE THAN 1 ROW ISSUE
Update V
	SET [Postcode]				=  W.[Postcode],
		[Huisnummer]			=  W.[Huisnummer],
		[Huisnummer toevoeging] =  W.[Huisnummer_toevoeging],
		IsDaeb					= COALESCE(LEFT(w.DAEB,1),'J')
	FROM [Toolbox_RM].[SRC].[Vhe] V
	INNER JOIN	[dbo].[WON_data] W
	ON	V.[Vhenummer]	= W.[Vhe_nummer]
	INNER JOIN (
				select (createdate), v.Vhenummer, ROW_NUMBER() OVER(PARTITION BY UploadID, VHe_nummer ORDER BY createdate desc) volgorde
				from  	[Toolbox_RM].[SRC].[Vhe] V
				INNER JOIN	[dbo].[WON_data] W
				ON			V.[Vhenummer]	= W.[Vhe_nummer]
				WHERE		V.[UploadID]	= @UploadID AND
				V.[BOG of WON]	= 'WON' ) T
			on v.Vhenummer = t.Vhenummer
			and w.createdate = t.createdate
			and t.volgorde = 1
WHERE		V.[UploadID]	= @UploadID AND
			V.[BOG of WON]	= 'WON'
				
-- SRC.VHE (Ruimte -> [BOG of WON]=BOG)
INSERT	[Toolbox_RM].[SRC].[Vhe] ([UploadID], [Vhenummer], [Postcode], [Straatnaam], [Huisnummer], [Huisnummer toevoeging], [Waarderingscomplex], [Financieel complex], [IsDaeb], [BOG of WON], [AantalVhe], [GBO oppervlakte per vhe], [Huurprijs per vhe per maand], [Leegwaarde per vhe], [Markthuur per vhe per maand], [Datum in exploitatie], [Datum uit exploitatie], [Reden mutatie])
SELECT	@UploadID,
		RTRIM(LEFT(VheNaam, CHARINDEX('-', VheNaam) - 1)) AS [Vhenummer],
		NULL AS [Postcode],
		LTRIM(SUBSTRING(VheNaam, CHARINDEX('-', VheNaam) + 1, 500)) AS [Straatnaam],
		NULL AS [Huisnummer],
		NULL AS [Huisnummer toevoeging],
		left(ComplexNaam,45) + right(ComplexNaam,5) AS [Waarderingscomplex],
		left(ComplexNaam,45) + right(ComplexNaam,5) AS [Financieel complex], --RTRIM(LEFT(ComplexNaam, CHARINDEX('-', ComplexNaam) - 1)) AS [Financieel complex],
		NULL AS [IsDaeb],
		'BOG' AS [BOG of WON],
		1 AS [AantalVhe],
		VVO AS [GBO oppervlakte per vhe],
		[Maandhuur] AS [Huurprijs per vhe per maand],
		[Leegwaarde] AS [Leegwaarde per vhe],
		[Mutatiehuur] AS [Markthuur per vhe per maand],
		NULL AS [Datum in exploitatie],
		NULL AS [Datum uit exploitatie],
		NULL AS [Reden mutatie]
FROM	(
SELECT	*,
		ROW_NUMBER() OVER (PARTITION BY VheNaam ORDER BY [Period] DESC) AS rownbr
FROM	(
			SELECT	cast(M.DoubleCol / 100 as int) AS [Period], CV.[Complex] AS ComplexNaam, V.[Name] AS VheNaam, V.[Label], V.DoubleCol
			FROM	[dbo].[RM_Reaturn_Model] M
			inner join [dbo].[RM_Reaturn_Ruimte] V 
				on M.FromSnapshotNumber between V.FromSnapshotNumber 
				and isnull(V.ThruSnapshotNumber, 999)
			inner join [dbo].[RM_Reaturn_Complex_eenheden] CV 
				on M.FromSnapshotNumber between CV.FromSnapshotNumber 
				and isnull(CV.ThruSnapshotNumber, 999)
				and V.[Name] = CV.VHE
			WHERE	2000 + cast(M.DoubleCol / 100 as int) = @Boekjaar
--			 AND	cast(M.DoubleCol as int) % 100 = 12
			 AND	V.[Label] in ('VVO', 'Maandhuur', 'Leegwaarde', 'Mutatiehuur', 'Boekwaarde')
			 
		) T
PIVOT
(
	MAX(DoubleCol)
	FOR [Label] in ([VVO], [Maandhuur], [Leegwaarde], [Mutatiehuur], [Boekwaarde] )
) AS pvt
) T
WHERE	rownbr = 1 and [Boekwaarde] <> 0


--Update House Address details for BOG : --Fixed, more than 1 row issue

Update V
	SET [Postcode]				=  B.[VHE_103_Postcode],
		[Huisnummer]			=  B.[VHE_105_Huisnummer],
		[Huisnummer toevoeging] =  B.[VHE_106_Huisnummer_toevoeging],
		[IsDaeb]				=  COALESCE(LEFT(B.VHE_503_IsDaeb,1),'J')
	FROM [Toolbox_RM].[SRC].[Vhe] V
	INNER JOIN	[dbo].[BOG_Data] B
	ON	V.[Vhenummer]	= B.VHE_101_Vhenummer
	INNER JOIN (
				select TaxatieRonde, v.Vhenummer, ROW_NUMBER() OVER(PARTITION BY UploadID, VHE_101_Vhenummer ORDER BY TaxatieRonde desc) volgorde
				from  	[Toolbox_RM].[SRC].[Vhe] V
				INNER JOIN	[dbo].[BOG_Data] B
				ON			V.[Vhenummer]	= B.VHE_101_Vhenummer
				WHERE		V.[UploadID]	= @UploadID AND
				V.[BOG of WON]	= 'BOG' ) T
			on v.Vhenummer = t.Vhenummer
			and B.TaxatieRonde = T.TaxatieRonde
			and T.volgorde = 1
WHERE		V.[UploadID]	= @UploadID AND
			V.[BOG of WON]	= 'BOG'
/*
Update V
	SET [Postcode]				=  W.[Postcode],
		[Huisnummer]			=  W.[Huisnummer],
		[Huisnummer toevoeging] =  W.[Huisnummer_toevoeging],
		IsDaeb					= COALESCE(LEFT(w.DAEB,1),'J')
	FROM [Toolbox_RM].[SRC].[Vhe] V
	INNER JOIN	[dbo].[WON_data] W
	ON	V.[Vhenummer]	= W.[Vhe_nummer]
	INNER JOIN (
				select (createdate), v.Vhenummer, ROW_NUMBER() OVER(PARTITION BY UploadID, VHe_nummer ORDER BY createdate desc) volgorde
				from  	[Toolbox_RM].[SRC].[Vhe] V
				INNER JOIN	[dbo].[WON_data] W
				ON			V.[Vhenummer]	= W.[Vhe_nummer]
				WHERE		V.[UploadID]	= @UploadID AND
				V.[BOG of WON]	= 'BOG' ) T
			on v.Vhenummer = t.Vhenummer
			and w.createdate = t.createdate
			and t.volgorde = 1
WHERE		V.[UploadID]	= @UploadID AND
			V.[BOG of WON]	= 'WON'*/


GO
